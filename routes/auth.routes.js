const {Router} = require('express')
const bcrypt = require('bcryptjs')
const {check, validationResult} = require('express-validator')
const config = require('config')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const router = Router()



// /api/auth
router.post('/register',
    [
        check('email', 'Некорреектный email').isEmail(),
        check('password', 'Минимальная длина пароля 6 символов')
            .isLength({min: 6})
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некоректные данные при регистрации'
                })
            }

            const {email, password} = req.body

            const candidate = await User.findOne({email})

            if (candidate) {
                return res.status(400).json({message: 'такой пользователь существует'})
            }

            const hashedPassword = await bcrypt.hash(password, 12)
            const user = new User({email, password: hashedPassword})

            await user.save()

            res.status(201).json({message: 'Пользовател создан'})
        } catch (e) {
            res.status(500).json({'message': 'Чтото пошло не так попробуйте снова'})
        }
    })

router.post('/login',
    [
        check('email','В ведите коректный email').normalizeEmail().isEmail(),
        check('password','Введите пароль').exists()
    ]
    , async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некоректные данные при входе в систму'
                })
            }

            const {email,password} = req.body

            const user = await  User.findOne({email})

            if (!user){
                return res.status(400).json({message:'Пользовательне найден'})
            }

            const isMatch= await bcrypt.compare(password,user.password)

            if(!isMatch){
                return res.status(400).json({message:'Неверный пароль попробуйте снова'})
            }

            const token = jwt.sign(
                { userId:user.id},
                config.get('jwtSecret'),
                {expiresIn: '1h'}
            )
            res.json({token,userId:user.id})
        } catch (e) {
            res.status(500).json({'message': 'Чтото пошло не так попробуйте снова' +e.message})
        }
    })

module.exports = router